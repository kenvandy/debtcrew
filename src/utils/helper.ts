export const hideNumber = (str: string) => str.replace(/^\d{0,4}/, (x) => x.replace(/./g, '*'));
