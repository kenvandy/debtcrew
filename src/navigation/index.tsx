import { BottomTabBarProps, createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import HomeScreen from '../screens/HomeScreen';
import DebitCardScreen from '../screens/DebitCardScreen';

import { TAB_NAVIGATION_ROOT } from './routes';
import PaymentScreen from '../screens/PaymentScreen';
import CreditScreen from '../screens/CreditScreen';
import ProfileScreen from '../screens/Profilescreen';
import navigationConfigs from './config';
import { BottomTabBar } from '../components';
import SpendingLimitScreen from '../screens/SpendingLimitScreen';
import { navigationRef } from './NavigationService';

export default function Navigation() {
  return (
    <NavigationContainer ref={navigationRef}>
      <RootNavigator />
    </NavigationContainer>
  );
}

const Stack = createNativeStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen
        name={TAB_NAVIGATION_ROOT.DEBIT_CARD_ROUTE.SPENDING_LIMIT}
        component={SpendingLimitScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

const BottomTab = createBottomTabNavigator();

function BottomTabNavigator() {
  const ArrayTabs = [
    {
      name: TAB_NAVIGATION_ROOT.HOME_ROUTE.HOME_ROOT,
      title: 'Home',
      component: HomeScreen,
      iconName: 'home',
      iconSize: 25,
    },
    {
      name: TAB_NAVIGATION_ROOT.DEBIT_CARD_ROUTE.DEBIT_CARD_ROOT,
      title: 'Debit Card',
      component: DebitCardScreen,
      iconName: 'pay',
      iconSize: 20,
    },
    {
      name: TAB_NAVIGATION_ROOT.PAYMENT_ROUTE.PAYMENT_ROOT,
      title: 'Payments',
      component: PaymentScreen,
      iconName: 'payments',
      iconSize: 26,
    },
    {
      name: TAB_NAVIGATION_ROOT.CREDIT_ROUTE.CREDIT_ROOT,
      title: 'Credit',
      component: CreditScreen,
      iconName: 'Credit',
      iconSize: 25,
    },
    {
      name: TAB_NAVIGATION_ROOT.PROFILE_ROUTE.PROFILE_ROOT,
      title: 'Profile',
      component: ProfileScreen,
      iconName: 'account',
      iconSize: 25,
    },
  ];

  return (
    <BottomTab.Navigator
      initialRouteName={TAB_NAVIGATION_ROOT.DEBIT_CARD_ROUTE.DEBIT_CARD_ROOT}
      screenOptions={navigationConfigs}
      tabBar={(props: BottomTabBarProps) => <BottomTabBar {...props} />}
    >
      {ArrayTabs.map((item, index) => (
        <BottomTab.Screen key={`${index}`} options={{ ...item }} {...item} />
      ))}
    </BottomTab.Navigator>
  );
}
