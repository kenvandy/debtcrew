import React from 'react';
import { ViewStyle } from 'react-native';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import { useFonts } from 'expo-font';

interface Props {
  name: string;
  size?: number;
  color?: string;
  onPress?: () => void;
  style?: ViewStyle;
}

const DCIcon = ({ name, size, color, onPress, style }: Props) => {
  const [fontsLoaded] = useFonts({ IcoMoon: require('../../assets/icomoon/icomoon.ttf') });

  if (!fontsLoaded) {
    return <></>;
  }

  const Icon = createIconSetFromIcoMoon(require('../../assets/icomoon/selection.json'), 'IcoMoon', 'Home.ttf');

  const icons: any = {};
  return icons[name] || <Icon name={name} size={size} color={color} onPress={onPress} style={style} />;
};

export default React.memo(DCIcon);
