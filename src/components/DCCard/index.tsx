import React, { useState, useCallback, useEffect } from 'react';
import { Pressable, StyleSheet, TouchableOpacity, View } from 'react-native';
import DCText from '../DCText';

import { DCIcon } from '..';
import { COMPANY_NAME } from '../../constants/System';
import { COLORS } from '../../constants/Colors';
import { Octicons } from '@expo/vector-icons';
import { hideNumber } from '../../utils/helper';

interface ICard {
  name: string;
  cardNumber: string[];
  expireTime: string;
  cvv: string;
}

const DCCard = ({ name, cardNumber, expireTime, cvv }: ICard) => {
  const [isHideCard, setHideCard] = useState(false);
  const [cardNum, setCardNum] = useState(cardNumber);
  const [cvvNum, setCVVNum] = useState(cvv);

  const hideCardNumber = useCallback(() => {
    const result = cardNum.map((val: string, index: number) => (index < 3 ? hideNumber(val) : val));
    setCardNum(result);
  }, []);

  const hideCVV = useCallback(() => {
    const result = hideNumber(cvv);
    setCVVNum(result);
  }, []);

  useEffect(() => {
    if (isHideCard) {
      hideCardNumber();
      hideCVV();
    } else {
      setCardNum(cardNumber);
      setCVVNum(cvv);
    }
  }, [isHideCard]);

  const renderCircle = useCallback((item: string) => {
    return item.split('').map((_, index) => (
      <View style={styles.circleWrapper} key={index}>
        <View style={styles.circle} />
      </View>
    ));
  }, []);

  const renderCardNumber = useCallback(
    () =>
      cardNum.map((item: string, index: number) => (
        <DCText key={index} style={styles.cardNumber} weight="semiBoldDemi">
          {item.includes('*') ? renderCircle(item) : item}
        </DCText>
      )),
    [cardNum, cardNumber],
  );

  const renderCvv = useCallback(
    () => (
      <View style={styles.cvvWrapper}>
        <DCText style={styles.cvv} weight="semiBoldDemi" size="smallX">
          CVV:
        </DCText>
        <DCText
          style={[
            styles.cvv,
            {
              marginTop: isHideCard ? -3 : 0,
              marginBottom: isHideCard ? -8 : 0,
            },
          ]}
          weight="semiBoldDemi"
          size={isHideCard ? 'xxl' : 'smallX'}
        >
          {cvvNum}
        </DCText>
      </View>
    ),
    [cardNum, cardNumber],
  );

  const renderShowHideCard = useCallback(
    () => (
      <TouchableOpacity onPress={() => setHideCard(!isHideCard)} style={styles.showHideWrapper} activeOpacity={0.9}>
        <Octicons name={isHideCard ? 'eye' : 'eye-closed'} color={COLORS.primary} size={15} />
        <DCText color={COLORS.primary} size="small" weight="medium" style={styles.showHideTitle}>
          {!isHideCard ? 'Hide' : 'Show'} card number
        </DCText>
      </TouchableOpacity>
    ),
    [isHideCard],
  );

  return (
    <View style={styles.container}>
      {renderShowHideCard()}
      <View style={styles.contentContainer}>
        {/* Company Name */}
        <View style={styles.logoWrapper}>
          <DCIcon name="home" size={24} color={COLORS.white} style={styles.logo} />
          <DCText style={styles.cardName} weight="semiBoldDemi" size="medium">
            {COMPANY_NAME}
          </DCText>
        </View>

        {/* Card Name */}
        <DCText style={styles.cardName} weight="bold" size="xl">
          {name}
        </DCText>

        {/* Card Numer */}
        <View style={styles.cardNumberWrapper}>{renderCardNumber()}</View>

        {/* Card Time and CVV */}
        <View style={styles.timeCvvWraper}>
          <DCText style={styles.timeCvv} weight="semiBoldDemi" size="smallX">
            Thru: {expireTime}
          </DCText>
          {renderCvv()}
        </View>

        {/* CARD TYPE */}
        <View style={styles.visaLogoWrapper}>
          <DCIcon name="visa" size={20} color={COLORS.white} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 25,
  },
  contentContainer: {
    backgroundColor: COLORS.primary,
    padding: 25,
    borderRadius: 10,
  },
  logoWrapper: {
    marginBottom: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  logo: {
    marginRight: 3,
  },
  cardName: {
    letterSpacing: 0.53,
  },
  cardNumberWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardNumber: {
    marginTop: 25,
    letterSpacing: 3,
    minWidth: 70,
  },
  timeCvvWraper: {
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    height: 20,
  },
  timeCvv: {
    marginRight: 30,
    letterSpacing: 1,
  },
  visaLogoWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 5,
  },
  showHideWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    alignSelf: 'flex-end',
    paddingHorizontal: 12,
    paddingTop: 8,
    paddingBottom: 15,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    // position: 'absolute',
    top: 10,
    width: 155,
    height: 40,
    marginTop: 100,
  },
  showHideTitle: {
    marginLeft: 5,
  },

  circleWrapper: {
    width: 12,
    marginTop: -5,
  },
  circle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: COLORS.white,
  },
  cvvWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cvv: {
    marginRight: 5,
    letterSpacing: 1,
  },
});

export default DCCard;
