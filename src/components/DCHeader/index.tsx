import { Feather } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { DCIcon, DCText } from '..';
import { COLORS } from '../../constants/Colors';
import { goBack } from '../../navigation/NavigationService';

interface IHeader {
  isBack?: boolean;
  headerName?: string;
}

const DCHeader = ({ isBack, headerName }: IHeader) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerWrapper}>
        {isBack ? <Feather name="chevron-left" size={26} color={COLORS.white} onPress={() => goBack()} /> : <View />}
        <DCIcon name="home" color={COLORS.primary} size={30} style={styles.icon} />
      </View>
      <DCText weight={'bold'} size={'xxl'} style={[styles.title, { marginTop: isBack ? 20 : 10 }]}>
        {headerName}
      </DCText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
  },
  title: {
    color: COLORS.white,
  },
  icon: {},
  headerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
  },
});

export default DCHeader;
