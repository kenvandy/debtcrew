import React, { Fragment } from 'react';
import { StyleSheet, View } from 'react-native';
import { COLORS } from '../../constants/Colors';

interface Props {
  children: React.ReactNode;
}

const DCMainLayout = ({ children }: Props) => {
  return (
    <Fragment>
      <View style={styles.container}>{children}</View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.blue1,
    paddingTop: 30,
  },
});

export default DCMainLayout;
