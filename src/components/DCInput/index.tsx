import React, { ReactElement, ReactNode, memo } from 'react';
import { KeyboardTypeOptions, StyleProp, StyleSheet, TextInput, TextStyle, View, ViewStyle } from 'react-native';
import { COLORS } from '../../constants/Colors';

export interface Props {
  customStyle?: StyleProp<TextStyle>;
  inputContainerStyle?: StyleProp<ViewStyle>;
  placeholder?: string;
  onChangeText?: (keyword: string) => void;
  icon?: ReactElement;
  rightText?: ReactElement;
  renderLeft?: () => ReactNode;
  defaultValue?: string;
  keyboardType?: KeyboardTypeOptions;
}

const DCInput = ({
  renderLeft,
  inputContainerStyle,
  customStyle,
  placeholder,
  onChangeText,
  icon,
  defaultValue,
  keyboardType = 'default',
  ...rest
}: Props) => {
  const renderLeftInput = () => {
    if (renderLeft) return renderLeft();
    return !!icon ? icon : null;
  };

  return (
    <View style={[styles.inputContainer, inputContainerStyle]}>
      {renderLeftInput()}
      <TextInput
        style={[styles.textInput, customStyle]}
        placeholder={placeholder}
        underlineColorAndroid={'transparent'}
        autoCorrect={false}
        defaultValue={defaultValue}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        {...rest}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    display: 'flex',
    flex: 1,
    width: '90%',
    height: '100%',
    color: COLORS.dark,
    paddingRight: 5,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.gray5,
  },
});
export default memo(DCInput);
