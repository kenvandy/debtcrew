import React, { memo, useState, useCallback, useMemo } from 'react';
import { StyleSheet, View, LayoutChangeEvent } from 'react-native';
import { DCText } from '..';
import { COLORS } from '../../constants/Colors';

interface Props {
  spent: number;
  limit: number;
}

const DCProgressLimit = ({ spent, limit }: Props) => {
  const [progressLayoutWidthWidth, setProgressLayoutWidth] = useState(0);

  const progressLayout = useCallback((event: LayoutChangeEvent) => {
    const { width } = event.nativeEvent.layout;
    setProgressLayoutWidth(width);
  }, []);

  const progressWidth = useMemo(
    () => (progressLayoutWidthWidth / limit / 100) * 100 * spent,
    [spent, limit, progressLayoutWidthWidth],
  );
  const styles = useStyles(progressWidth, progressLayoutWidthWidth);

  return (
    <View style={styles.container}>
      <View style={styles.titleWrapper}>
        <DCText color={COLORS.dark} weight="medium" size="smallX">
          Debit card spending limit
        </DCText>

        <View style={styles.spendingWrapper}>
          <DCText color={COLORS.primary} size="smallX" weight="medium">
            ${spent}
          </DCText>
          <DCText color={COLORS.gray7} size="smallX" weight="medium">
            {'  '}| {'  '}${limit.toLocaleString()}
          </DCText>
        </View>
      </View>
      <View style={styles.progressWrapper} onLayout={progressLayout}>
        <View style={styles.progress}>
          {progressWidth < progressLayoutWidthWidth && <View style={styles.triangle} />}
        </View>
      </View>
    </View>
  );
};

const useStyles = (progressWidth: number, progressLayoutWidthWidth: number) =>
  StyleSheet.create({
    container: {
      marginTop: 10,
    },
    titleWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },

    spendingWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },

    progressWrapper: {
      borderRadius: 30,
      height: 15,
      backgroundColor: COLORS.subPrimary,
      marginTop: 6,
      marginRight: 100,
      width: '100%',
    },
    progress: {
      backgroundColor: COLORS.primary,
      width: progressWidth,
      height: 15,
      borderTopLeftRadius: 30,
      borderBottomLeftRadius: 30,
      borderTopRightRadius: progressWidth === progressLayoutWidthWidth ? 30 : 0,
      borderBottomRightRadius: progressWidth === progressLayoutWidthWidth ? 30 : 0,
      marginRight: 100,
      alignItems: 'flex-end',
    },
    triangle: {
      width: 0,
      height: 0,
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderLeftWidth: 5,
      borderBottomWidth: 15,
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderBottomColor: COLORS.subPrimary,
    },
  });

export default memo(DCProgressLimit);
