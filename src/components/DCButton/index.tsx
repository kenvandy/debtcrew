import React, { memo, ReactNode } from 'react';
import { Pressable, StyleSheet, ViewStyle } from 'react-native';
import { COLORS } from '../../constants/Colors';

interface Props {
  size?: 'big' | 'medium' | 'small';
  children?: string | ReactNode;
  disabled?: boolean;
  onPress?: () => void;
  style?: ViewStyle;
}

const DCButton = ({ disabled, style, children, onPress, ...rest }: Props) => {
  const styles = useStyles(disabled);
  return (
    <Pressable {...rest} disabled={disabled} onPress={onPress} style={[styles.container, style]}>
      {children}
    </Pressable>
  );
};

const useStyles = (disabled?: boolean) =>
  StyleSheet.create({
    container: {
      backgroundColor: disabled ? COLORS.gray5 : COLORS.primary,
      height: 56,
      width: '100%',
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center',
      shadowColor: COLORS.black12,
      shadowRadius: 4,
      elevation: 6,
      shadowOpacity: 1,
      shadowOffset: {
        width: 0,
        height: 2,
      },
    },
  });

export default memo(DCButton);
