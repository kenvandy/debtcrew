import { StyleSheet } from 'react-native';
import { COLORS } from '../../constants/Colors';

export const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  toggleButton: {
    width: 44,
    height: 24,
    backgroundColor: COLORS.gray5,
    borderRadius: 12,
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  toggleCircle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: COLORS.white,
    position: 'absolute',
    shadowColor: COLORS.gray1,
    shadowRadius: 4,
    elevation: 2,
    shadowOpacity: 1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  unToggledCircle: {
    left: 1,
  },
  toggledCircle: {
    right: 1,
  },
  toggedButton: {
    backgroundColor: COLORS.primary,
  },
});
