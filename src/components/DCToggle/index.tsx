import React, { memo, useEffect } from 'react';
import { Pressable, View, ViewStyle } from 'react-native';
import { styles } from './styled';

interface Props {
  defaultValue: boolean;
  onToggled?: (isToglge: boolean) => void;
  style?: ViewStyle;
  buttonStyle?: ViewStyle;
  circleStyle?: ViewStyle;
}

const DCToggle = ({ defaultValue = false, onToggled, style, buttonStyle, circleStyle, ...rest }: Props) => {
  const [isToggled, setToggled] = React.useState(defaultValue);

  useEffect(() => {
    if (defaultValue) {
      setToggled(true);
    } else {
      setToggled(false);
    }
  }, [defaultValue]);

  const _onPress = () => {
    setToggled(!isToggled);

    if (onToggled) onToggled(!isToggled);
  };

  return (
    <View style={[styles.wrapper, style]}>
      <Pressable
        style={[styles.toggleButton, buttonStyle, isToggled && styles.toggedButton]}
        onPress={_onPress}
        {...rest}
      >
        <View style={[styles.toggleCircle, circleStyle, isToggled ? styles.toggledCircle : styles.unToggledCircle]} />
      </Pressable>
    </View>
  );
};

export default memo(DCToggle);
