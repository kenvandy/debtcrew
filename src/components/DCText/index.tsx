import React, { memo } from 'react';
import { StyleSheet, Text as RNText, TextProps } from 'react-native';
import { COLORS } from '../../constants/Colors';

interface Props extends TextProps {
  children: string | string[] | any;
  size?: 'xxs' | 'xs' | 'small' | 'default' | 'large' | 'xl' | 'xxl' | 'medium' | 'smallX' | 'xxxl';
  weight?: 'regular' | 'medium' | 'semibold' | 'bold' | 'boldDemi' | 'semiBoldDemi';
  color?: string;
}

const useStyles = (color: string) =>
  StyleSheet.create({
    defaultTextStyle: {
      color: color,
    },
  });

const fontSize = StyleSheet.create({
  default: { fontSize: 14 },
  large: { fontSize: 20 },
  medium: { fontSize: 18 },
  small: { fontSize: 12 },
  smallX: { fontSize: 13 },
  xxs: { fontSize: 8 },
  xs: { fontSize: 10 },
  xl: { fontSize: 22 },
  xxl: { fontSize: 24 },
  xxxl: { fontSize: 20 },
});

const fontWeight = StyleSheet.create({
  regular: { fontFamily: 'AvenirNextLTPro-Regular', fontWeight: 'normal' },
  medium: { fontFamily: 'AvenirNextLTPro-Medium', fontWeight: '300' },
  semibold: { fontFamily: 'AvenirNextLTPro-Bold', fontWeight: '600' },
  bold: { fontFamily: 'AvenirNextLTPro-Bold', fontWeight: 'bold' },
  boldDemi: { fontFamily: 'AvenirNextLTPro-Heavy', fontWeight: '300' },
  semiBoldDemi: { fontFamily: 'AvenirNextLTPro-Demi', fontWeight: '700' },
});

const SPText = ({ color = COLORS.white, weight = 'regular', size = 'default', children, style, ...rest }: Props) => {
  const styleFontSize = fontSize[size];
  const styleFontWeight = fontWeight[weight];
  return (
    <RNText
      style={[useStyles(color).defaultTextStyle, styleFontSize, styleFontWeight, style]}
      allowFontScaling={false}
      {...rest}
    >
      {children}
    </RNText>
  );
};

export default memo(SPText);
