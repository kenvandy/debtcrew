import React, { memo } from 'react';
import { StyleSheet, View, Pressable, Platform } from 'react-native';
import DCText from '../DCText';
import DCIcon from '../DCIcon';
import { COLORS } from '../../constants/Colors';

interface Props {
  state: any;
  descriptors: any;
  navigation: any;
}

const BottomTabBar = ({ state, descriptors, navigation }: Props) => {
  const renderTabItem = (route: any, index: number) => {
    {
      const { options } = descriptors[route.key];

      const isFocused = state.index === index;

      const onPress = () => {
        const event = navigation.emit({
          type: 'tabPress',
          target: route.key,
          canPreventDefault: true,
        });

        if (!isFocused && !event.defaultPrevented) {
          navigation.navigate({ name: route.name, merge: true });
        }
      };

      return (
        <Pressable style={styles.tabItem} key={`tab-item--${index}-->`} onPress={onPress}>
          <DCIcon name={options?.iconName} size={options?.iconSize} color={isFocused ? COLORS.primary : COLORS.gray4} />
          <DCText
            size={'xs'}
            weight="medium"
            style={{
              color: isFocused ? COLORS.primary : COLORS.gray4,
              marginTop: 6,
            }}
          >
            {options?.title}
          </DCText>
        </Pressable>
      );
    }
  };

  return (
    <View style={styles.tabWrapper}>
      {state.routes.map((route: any, index: number) => renderTabItem(route, index))}
    </View>
  );
};

const styles = StyleSheet.create({
  tabWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: Platform.OS === 'ios' ? 90 : 70,
    paddingBottom: Platform.OS === 'ios' ? 20 : 0,
  },
  tabItem: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

export default memo(BottomTabBar);
