export const CUSTOM_FONTS = {
  'AvenirNextLTPro-Regular': require('../assets/fonts/AvenirNextLTPro-Regular.otf'),
  'AvenirNextLTPro-Medium': require('../assets/fonts/AvenirNextLTPro-Medium.otf'),
  'AvenirNextLTPro-Bold': require('../assets/fonts/AvenirNextLTPro-Bold.otf'),
  'AvenirNextLTPro-Heavy': require('../assets/fonts/AvenirNextLTPro-Heavy.otf'),
  'AvenirNextLTPro-Demi': require('../assets/fonts/AvenirNextLTPro-Demi.otf'),
};
