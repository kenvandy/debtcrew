import React from 'react';
import { DCMainLayout, DCHeader } from '../components';

const PaymentScreen = () => {
  return (
    <DCMainLayout>
      <DCHeader headerName="Payment" />
    </DCMainLayout>
  );
};

export default PaymentScreen;
