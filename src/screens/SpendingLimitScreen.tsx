import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import { Keyboard, StyleSheet, TouchableWithoutFeedback, View, Pressable } from 'react-native';
import { DCButton, DCIcon, DCInput, DCMainLayout, DCText } from '../components';
import DCHeader from '../components/DCHeader';
import { COLORS } from '../constants/Colors';
import { goBack } from '../navigation/NavigationService';
import { mockDebit } from '../services/mockDebit';
import { setSpendingLimitAmount } from '../store/SpendingLimit/spendingLimit.action';

interface ILimitRange {
  key: string;
  value: string;
}

const MAXIMUM_SPENDING = 20000;

const SpendingLimit = () => {
  const [limitValue, setLimitValue] = useState(0);
  const dispatch = useDispatch();

  const renderInputLeft = useCallback(() => {
    return (
      <View style={styles.currencyWrapper}>
        <DCText weight="bold" size="small">
          S$
        </DCText>
      </View>
    );
  }, []);

  const handleOnChangeValue = useCallback((val: string) => {
    const parseVal = parseFloat(val);
    if (parseVal > MAXIMUM_SPENDING || parseVal < mockDebit.personalFinanceStatus.spent) {
      setLimitValue(0);
    } else {
      setLimitValue(parseVal);
    }
  }, []);

  const renderMoneyInput = useCallback(() => {
    return (
      <View>
        <DCInput
          defaultValue={limitValue > 0 ? limitValue.toLocaleString() : ''}
          renderLeft={renderInputLeft}
          inputContainerStyle={styles.inputContainer}
          keyboardType="numeric"
          customStyle={styles.inputStyle}
          onChangeText={handleOnChangeValue}
        />
        <DCText color={COLORS.gray6} size={'smallX'}>
          Here weekly means the last 7 days - not the calendar week Spending limit should be equal or greater than the
          spent
        </DCText>
      </View>
    );
  }, [handleOnChangeValue, limitValue, renderInputLeft]);

  const renderMoneyRange = useCallback(() => {
    return (
      <View style={styles.rangeWrapper}>
        {mockDebit.limitRange.map((item: ILimitRange) => (
          <Pressable key={item.key} style={styles.rangeItem} onPress={() => setLimitValue(Number(item.key))}>
            <DCText color={COLORS.primary} weight="semiBoldDemi" size={'smallX'}>
              S$ {item.value}
            </DCText>
          </Pressable>
        ))}
      </View>
    );
  }, []);

  const renderContent = useCallback(() => {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.container}>
          <View style={styles.row}>
            <DCIcon name={'weeklyLine'} color={COLORS.blue1} size={17} />
            <DCText style={styles.title} weight="medium">
              Set a weekly debit card spending limit
            </DCText>
          </View>
          {renderMoneyInput()}
          {renderMoneyRange()}
        </View>
      </TouchableWithoutFeedback>
    );
  }, [renderMoneyInput, renderMoneyRange]);

  const handleSaveLimit = useCallback(() => {
    dispatch(setSpendingLimitAmount(limitValue));
    goBack();
  }, [dispatch, limitValue]);

  return (
    <DCMainLayout>
      <DCHeader headerName="Spending limit" isBack />
      {renderContent()}
      <View style={styles.buttonWrapper}>
        <DCButton disabled={!limitValue} style={styles.button} onPress={handleSaveLimit}>
          <DCText size="medium" weight="medium">
            Save
          </DCText>
        </DCButton>
      </View>
    </DCMainLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    flex: 1,
    marginTop: 40,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: 25,
    paddingHorizontal: 25,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    color: COLORS.dark,
    marginLeft: 10,
  },
  currencyWrapper: {
    backgroundColor: COLORS.primary,
    marginRight: 10,
    paddingHorizontal: 13,
    paddingVertical: 5,
    borderRadius: 5,
  },
  inputContainer: {
    marginTop: 10,
    marginBottom: 10,
  },
  rangeWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 25,
    width: '100%',
    borderRadius: 4,
  },
  rangeItem: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.subPrimary,
    width: '30%',
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
  buttonWrapper: {
    position: 'absolute',
    bottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  button: {
    width: '80%',
  },
  inputStyle: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'AvenirNextLTPro-Bold',
  },
});

export default SpendingLimit;
