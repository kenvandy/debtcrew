import React, { useCallback, useState, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Image } from 'react-native';
import { useSelector } from 'react-redux';
import { DCCard, DCMainLayout, DCProgressLimit, DCText, DCToggle } from '../components';
import DCHeader from '../components/DCHeader';

import { COLORS } from '../constants/Colors';
import { navigate } from '../navigation/NavigationService';
import { TAB_NAVIGATION_ROOT } from '../navigation/routes';
import { mockDebit } from '../services/mockDebit';
import { RootState } from '../store/reducers';

const { cardInfo } = mockDebit;

interface IMenu {
  id: number;
  title: string;
  subTitle: string;
  icon: string;
  isToggle?: boolean;
}

const DebitCardScreen = () => {
  const [toggledLimit, setToggleLimit] = useState(false);

  const currentLimitAmount = useSelector((state: RootState) => state?.SpendingLimit?.limitAmount);

  useEffect(() => {
    if (toggledLimit) {
      navigate(TAB_NAVIGATION_ROOT.DEBIT_CARD_ROUTE.SPENDING_LIMIT);
    }
  }, [toggledLimit]);

  const renderBalance = useCallback(
    () => (
      <View style={styles.balanceWrapper}>
        <DCText weight="semiBoldDemi">Available balance</DCText>
        <View style={styles.remainingWrapper}>
          <View style={styles.currencyWrapper}>
            <DCText weight="bold" size="small">
              S$
            </DCText>
          </View>
          <DCText weight="bold" size="xxl">
            {mockDebit.personalFinanceStatus.availableBalance.toLocaleString()}
          </DCText>
        </View>
      </View>
    ),
    [],
  );

  const renderCard = useCallback(
    () => (
      <View style={styles.cardWrapper}>
        <DCCard
          name={cardInfo.name}
          cardNumber={cardInfo.cardNumber}
          expireTime={cardInfo.expireTime}
          cvv={cardInfo.cvv}
        />
      </View>
    ),
    [],
  );

  const renderMenu = useCallback(() => {
    return (
      <View style={styles.menuWrapper}>
        {toggledLimit && <DCProgressLimit spent={mockDebit.personalFinanceStatus.spent} limit={currentLimitAmount} />}

        {mockDebit.menu.map((item: IMenu) => (
          <View key={item.id} style={styles.menuItemWrapper}>
            <View style={styles.row}>
              <Image source={item.icon} />

              <View style={styles.titleWrapper}>
                <DCText style={styles.menuTitle}>{item.title}</DCText>
                <DCText style={styles.menuSubTitle}>{item.subTitle}</DCText>
              </View>
            </View>

            {item?.isToggle && (
              <DCToggle defaultValue={false} onToggled={() => item.id === 1 && setToggleLimit(!toggledLimit)} />
            )}
          </View>
        ))}
      </View>
    );
  }, [currentLimitAmount, toggledLimit]);

  return (
    <DCMainLayout>
      <ScrollView
        style={styles.container}
        stickyHeaderIndices={[0]}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
        removeClippedSubviews={false}
      >
        <View>
          <DCHeader headerName="Debit Card" />
          {renderBalance()}
        </View>
        {renderCard()}

        <View style={styles.contentWrapper}>{renderMenu()}</View>
      </ScrollView>
    </DCMainLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingBottom: 80,
  },
  balanceWrapper: {
    paddingHorizontal: 25,
    marginTop: 25,
  },
  remainingWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  contentWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 120,
    zIndex: 999,
  },
  currencyWrapper: {
    backgroundColor: COLORS.primary,
    marginRight: 10,
    paddingHorizontal: 13,
    paddingVertical: 5,
    borderRadius: 5,
  },
  cardWrapper: {
    position: 'absolute',
    width: '100%',
    zIndex: 9999,
    borderRadius: 10,
    marginTop: 60,
  },
  menuWrapper: {
    backgroundColor: COLORS.white,
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    justifyContent: 'center',
    paddingTop: 80,
    paddingHorizontal: 20,
  },
  menuItemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 25,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleWrapper: {
    marginLeft: 10,
  },
  menuTitle: {
    color: COLORS.blue2,
  },
  menuSubTitle: {
    color: COLORS.dark40,
    marginTop: 5,
  },
});

export default DebitCardScreen;
