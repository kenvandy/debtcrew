import React from 'react';
import { DCMainLayout, DCHeader } from '../components';

const CreditScreen = () => {
  return (
    <DCMainLayout>
      <DCHeader headerName="Credit" />
    </DCMainLayout>
  );
};

export default CreditScreen;
