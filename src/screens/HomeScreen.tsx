import * as React from 'react';
import { DCMainLayout, DCHeader } from '../components';

export default function HomeScreen() {
  return (
    <DCMainLayout>
      <DCHeader headerName="Home" />
    </DCMainLayout>
  );
}
