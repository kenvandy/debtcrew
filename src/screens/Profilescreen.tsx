import React from 'react';
import { DCMainLayout, DCHeader } from '../components';

const ProfileScreen = () => {
  return (
    <DCMainLayout>
      <DCHeader headerName="Profile" />
    </DCMainLayout>
  );
};

export default ProfileScreen;
