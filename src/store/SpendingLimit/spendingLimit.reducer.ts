import { SET_SPENDING_LIMIT_AMOUNT } from './spendingLimit.action';

const spendingLimitState = {
  limitAmount: 5000,
};

function spendingLimitReducer(state = spendingLimitState, action: any) {
  switch (action.type) {
    case SET_SPENDING_LIMIT_AMOUNT:
      return {
        ...state,
        limitAmount: action?.data,
      };
    default:
      return {
        ...state,
      };
  }
}

export default spendingLimitReducer;
