const SET_SPENDING_LIMIT_AMOUNT = 'SET_SPENDING_LIMIT_AMOUNT';

const setSpendingLimitAmount = function (data: number, callback?: any) {
  return {
    type: SET_SPENDING_LIMIT_AMOUNT,
    data,
    callback,
  };
};

export { SET_SPENDING_LIMIT_AMOUNT, setSpendingLimitAmount };
