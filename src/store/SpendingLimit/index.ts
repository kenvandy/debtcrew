import { takeLatest } from 'redux-saga/effects';
import { SET_SPENDING_LIMIT_AMOUNT } from './spendingLimit.action';
import { getSpendingLimit } from './spendingLimit.sagas';

export default function* spendingLimitSaga() {
  yield takeLatest(SET_SPENDING_LIMIT_AMOUNT, getSpendingLimit);
}
