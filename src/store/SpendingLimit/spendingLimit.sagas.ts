import { call } from 'redux-saga/effects';
import { setSpendingLimitAmount } from './spendingLimit.action';

type SpendingParams = { data: number; type: string };

export function* getSpendingLimit({ data }: SpendingParams) {
  try {
    // TODO WE CAN DO SOMETHING WITH DATA AS MIDDLEWARE
    yield call(setSpendingLimitAmount, data);
  } catch (error) {
    console.log('GET SPENDING LIMIT ERROR', error);
  }
}
