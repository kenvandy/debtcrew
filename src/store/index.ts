import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducers from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const enhancer = compose(
  applyMiddleware(sagaMiddleware),
  // NOTE uncomment turn on to view the redux logger
  // applyMiddleware(logger),
);

const store = createStore(rootReducers, enhancer);

sagaMiddleware.run(rootSaga);

export { store };
