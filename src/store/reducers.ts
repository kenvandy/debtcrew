import { combineReducers } from 'redux';
import spendingLimitReducer from './SpendingLimit/spendingLimit.reducer';

const rootReducers = combineReducers({
  SpendingLimit: spendingLimitReducer,
});

export type RootState = ReturnType<typeof rootReducers>;

export default rootReducers;
