import { useFonts } from 'expo-font';
import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { LogBox } from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { CUSTOM_FONTS } from './src/constants/Fonts';

import useCachedResources from './src/hooks/useCachedResources';
import Navigation from './src/navigation';
import { store } from './src/store';
LogBox.ignoreLogs(['Non-serializable values were found in the navigation state']);

export default function App() {
  const isLoadingComplete = useCachedResources();

  useFonts(CUSTOM_FONTS);

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <ReduxProvider store={store}>
        <SafeAreaProvider>
          <Navigation />
        </SafeAreaProvider>
      </ReduxProvider>
    );
  }
}
