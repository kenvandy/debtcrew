# DebtCrew

## What is Debt Crew?

1. Debt Crew help you to manage you debt list conveniently. You will never forget who and how much you owe.
2. We also help you to manage your spending. So you will never overspend. Remember, you have debts to pay!!!

## As user I want

✅ Managing my Debt List to keep track the debts
✅ Limiting my spending weekly
✅ Freezing my credit card
✅ Managing my daily, weekly, monthly and yearly spending

## How to run the project

```
cd debtcrew
yar install
yarn ios or yarn android to run the project for the iOS and Android respectively
yarn lint to run eslint
```

## Structure

This project using Expo. I structure folder as follow:

1. Assets

- Contain fonts, images and icon.
- For the icon, I export svg and use iconmoon

2. Components

- Contain common components like Button, Text, Input,... to use accorss the project
- For the bigger project, I will use Atom design

3. Constants

- Contain fixed Colors, Fonts and some Sytems configurations

4. Hooks

- Custom hooks

5. Navigation

- Structure navigation for the project

6. Screens

- Contain all screens for this project and define in the navigation

1. Services

- For the mock data and API processing

8. Store

- For Redux and Saga
- This project doesn't need Redux, I setup basic thing to demonstrate the concept.

9. Utils

- For helpers functions and other utility tools.

## TODO NEXT

I plan to write e2e and unit test using Detox and jest.
but it's out of time
